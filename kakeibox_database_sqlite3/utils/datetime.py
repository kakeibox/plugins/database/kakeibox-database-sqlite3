from datetime import datetime


def from_timestamp_to_datetime(timestamp):
    return datetime.fromtimestamp(timestamp)
