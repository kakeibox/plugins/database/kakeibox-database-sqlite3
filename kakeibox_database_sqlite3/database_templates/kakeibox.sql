BEGIN TRANSACTION;

CREATE TABLE IF NOT EXISTS "kakeibox_core_account" (
	"id"	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	"uuid"	TEXT(36) NOT NULL UNIQUE,
	"name"	TEXT(256) NOT NULL,
	"description"	TEXT
);
INSERT INTO "main"."kakeibox_core_account"
("id", "uuid", "name", "description")
VALUES (1, '205d912b-0f5c-4e3f-b7c2-075ed76474de', 'Personal', 'Personal account');

CREATE TABLE IF NOT EXISTS "kakeibox_core_transaction" (
	"id"	INTEGER NOT NULL UNIQUE,
	"transaction_subcategory_id"	INTEGER NOT NULL,
	"transaction_type_id"	INTEGER NOT NULL,
	"uuid"	TEXT(36) NOT NULL UNIQUE,
	"description"	TEXT,
	"reference_number"	TEXT(128),
	"amount"	REAL NOT NULL,
	"record_hash"	TEXT(64) NOT NULL,
	"timestamp"	INTEGER NOT NULL,
	"account_id"	INTEGER NOT NULL,
	FOREIGN KEY("transaction_type_id") REFERENCES "kakeibox_core_transaction_type"("id"),
	PRIMARY KEY("id"),
	FOREIGN KEY("transaction_subcategory_id") REFERENCES "kakeibox_core_transaction_subcategory"("id"),
	FOREIGN KEY("account_id") REFERENCES "kakeibox_core_account"("id")
);
CREATE TABLE IF NOT EXISTS "kakeibox_core_transaction_category" (
	"id"	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	"code"	TEXT(3) NOT NULL UNIQUE,
	"name"	TEXT(128) NOT NULL
);
CREATE TABLE IF NOT EXISTS "kakeibox_core_transaction_type" (
	"id"	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	"code"	TEXT(3) NOT NULL UNIQUE,
	"name"	TEXT(128) NOT NULL
);
CREATE TABLE IF NOT EXISTS "kakeibox_core_transaction_subcategory" (
	"id"	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	"transaction_category_id"	INTEGER NOT NULL,
	"code"	TEXT(3) NOT NULL UNIQUE,
	"name"	TEXT(128) NOT NULL
);
INSERT INTO "kakeibox_core_transaction_category" ("id","code","name") VALUES (1,'SUR','Survival'),
 (2,'OPT','Optional'),
 (3,'CUL','Culture'),
 (4,'EXT','Extra');
INSERT INTO "kakeibox_core_transaction_type" ("id","code","name") VALUES (1,'EXP','Expense'),
 (2,'INC','Income');
INSERT INTO "kakeibox_core_transaction_subcategory" ("id","transaction_category_id","code","name") VALUES (1,1,'FOD','Food'),
 (2,1,'REN','Rent'),
 (3,1,'TRA','Transport'),
 (4,1,'KID','Kids'),
 (5,2,'RES','Restarurant'),
 (6,2,'SHO','Shopping'),
 (7,3,'BOK','Books'),
 (8,3,'MUS','Music'),
 (9,3,'SHW','Shows'),
 (10,3,'MOV','Movies'),
 (11,3,'MAG','Magazines'),
 (12,4,'GIF',' Gifts'),
 (13,4,'REP','Repairs'),
 (14,4,'FUR','Furniture');
COMMIT;
